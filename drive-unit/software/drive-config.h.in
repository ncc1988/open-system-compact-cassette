/*
 * This file is part of open-system-compact-cassette
 *
 * Copyright (C) 2019-2020 Moritz Strohm
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http s ://www.gnu.org/licenses/>.
 */


#ifndef DRIVE_CONFIG_H
#define DRIVE_CONFIG_H


// This file contains the hard-coded drive configuration. You need to copy this
// file to drive-config.h and edit the following parameters according to the
// capabilities of your drive.


// Drive capabilities:


/**
 * Whether the drive has the ability to detect the end of tape (EOT).
 * This can be a switch that is pressed when the end of one side of the cassette
 * is reached.
 */
constexpr bool DRIVECONFIG_CAN_DETECT_EOT = true;

/**
 * This parameter is for drives where the EOT switch is released (logical false)
 * when the end of tape is reached and otherwise pressed (logical true).
 */
constexpr bool DRIVECONFIG_DETECT_EOT_INVERSE_LOGIC = false;

/**
 * Whether the drive has the ability to fast forward (true) or not (false).
 */
constexpr bool DRIVECONFIG_HAS_FFORWARD = true;

/**
 * Whether the drive has the ability to control the fast forward function by
 * software (true) or not (false). The latter means that the fast forward
 * switch is a pure mechanical switch that cannot be controlled by software.
 */
constexpr bool DRIVECONFIG_CAN_CONTROL_FFORWARD = true;

/**
 * Whether the drive has the ability to fast reverse (true) or not (false).
 */
constexpr bool DRIVECONFIG_HAS_FREVERSE = true;

/**
 * Whether the drive has the ability to control the fast reverse function by
 * software (true) or not (false). The latter means that the fast forward
 * switch is a pure mechanical switch that cannot be controlled by software.
 */
constexpr bool DRIVECONFIG_CAN_CONTROL_FREVERSE = true;

/**
 * Whether the drive has the ability to play a cassette in reverse direction
 * (true) or not (false).
 */
constexpr bool DRIVECONFIG_CAN_REVERSE = true;

/**
 * Whether the drive has the ability to detect reverse playback (true)
 * or not (false).
 */
constexpr bool DRIVECONFIG_CAN_DETECT_REVERSE = true;

/**
 * If the drive has a switch that is pressed when playing in forward
 * direction and released when playing in reverse direction, this
 * must be set to true.
 */
constexpr bool DRIVECONFIG_DETECT_REVERSE_INVERSE_LOGIC = true;

/**
 * Whether the drive can control the reverse function (true) or not (false).
 * The latter means that reverse playback is toggled by a pure mechanical
 * switch that is not controllable by software.
 */
constexpr bool DRIVECONFIG_CAN_CONTROL_REVERSE = true;

/**
 * Whether the drive has the ability to detect movement of the takeup wheel
 * (true) or not (false).
 */
constexpr bool DRIVECONFIG_CAN_DETECT_TAKEUP_MOVEMENT = true;

/**
 * Whether the drive has the ability to detect movement of the takeup wheel
 * in reverse direction (true) or not (false).
 */
constexpr bool DRIVECONFIG_CAN_DETECT_REVERSE_TAKEUP_MOVEMENT = true;


// Pin configuration:


/**
 * The microcontroller pin where the EOT detector is connected to.
 */
constexpr uint8_t DRIVECONFIG_PIN_EOT = 0;

/**
 * The microcontroller pin where the cassette loading detector is connected to.
 */
constexpr uint8_t DRIVECONFIG_PIN_TAPE_LOADED = 0;

/**
 * The microcontroller pin where the fast forward detector is connected to.
 */
constexpr uint8_t DRIVECONFIG_PIN_FFORWARD = 0;

/**
 * The microcontroller pin where the fast reverse detector is connected to.
 */
constexpr uint8_t DRIVECONFIG_PIN_FREVERSE = 0;

/**
 * The microcontroller pin where the reverse detector is connected to.
 */
constexpr uint8_t DRIVECONFIG_PIN_REVERSE = 0;



#endif
