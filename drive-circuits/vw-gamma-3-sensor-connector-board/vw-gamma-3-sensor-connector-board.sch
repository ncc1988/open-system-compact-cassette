EESchema Schematic File Version 4
LIBS:vw-gamma-3-sensor-connector-board-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "VW Gamma 3 tape drive sensor board"
Date "2021-06-26"
Rev ""
Comp "open-system-compact-cassette project"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x10 J1
U 1 1 60D798D5
P 7900 5100
F 0 "J1" V 8024 5046 50  0000 C CNN
F 1 "Conn_01x10" V 8115 5046 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x10_P2.54mm_Vertical" H 7900 5100 50  0001 C CNN
F 3 "~" H 7900 5100 50  0001 C CNN
	1    7900 5100
	0    -1   1    0   
$EndComp
$Comp
L Connector_Generic:Conn_01x08 J2
U 1 1 60D799E3
P 4200 3400
F 0 "J2" H 4120 2775 50  0000 C CNN
F 1 "Conn_01x08" H 4120 2866 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x08_P2.54mm_Horizontal" H 4200 3400 50  0001 C CNN
F 3 "~" H 4200 3400 50  0001 C CNN
	1    4200 3400
	-1   0    0    1   
$EndComp
$Comp
L Switch:SW_Push_Open SW4
U 1 1 60D79B75
P 8150 1900
F 0 "SW4" H 8150 2115 50  0000 C CNN
F 1 "SW_Push_Open" H 8150 2024 50  0000 C CNN
F 2 "" H 8150 2100 50  0001 C CNN
F 3 "" H 8150 2100 50  0001 C CNN
	1    8150 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 3700 4750 3700
Wire Wire Line
	8000 3700 8000 4900
Wire Wire Line
	4400 3000 4750 3000
Wire Wire Line
	4750 3000 4750 2250
$Comp
L Device:R R1
U 1 1 60D79DE9
P 7300 1650
F 0 "R1" V 7093 1650 50  0000 C CNN
F 1 "100k" V 7184 1650 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 7230 1650 50  0001 C CNN
F 3 "~" H 7300 1650 50  0001 C CNN
	1    7300 1650
	0    1    1    0   
$EndComp
Connection ~ 4750 2250
Wire Wire Line
	8500 1900 8350 1900
$Comp
L Device:R R4
U 1 1 60D79FD8
P 5900 2550
F 0 "R4" H 5830 2504 50  0000 R CNN
F 1 "100k" H 5830 2595 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 5830 2550 50  0001 C CNN
F 3 "~" H 5900 2550 50  0001 C CNN
	1    5900 2550
	-1   0    0    1   
$EndComp
$Comp
L Device:R R5
U 1 1 60D7A09E
P 6250 2550
F 0 "R5" H 6180 2504 50  0000 R CNN
F 1 "100k" H 6180 2595 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 6180 2550 50  0001 C CNN
F 3 "~" H 6250 2550 50  0001 C CNN
	1    6250 2550
	-1   0    0    1   
$EndComp
$Comp
L Device:R R6
U 1 1 60D7A0CA
P 6600 2550
F 0 "R6" H 6530 2504 50  0000 R CNN
F 1 "100k" H 6530 2595 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 6530 2550 50  0001 C CNN
F 3 "~" H 6600 2550 50  0001 C CNN
	1    6600 2550
	-1   0    0    1   
$EndComp
$Comp
L Device:R R7
U 1 1 60D7A0FC
P 6950 2550
F 0 "R7" H 6880 2504 50  0000 R CNN
F 1 "100k" H 6880 2595 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 6880 2550 50  0001 C CNN
F 3 "~" H 6950 2550 50  0001 C CNN
	1    6950 2550
	-1   0    0    1   
$EndComp
$Comp
L Device:R R8
U 1 1 60D7A128
P 7300 2550
F 0 "R8" H 7230 2504 50  0000 R CNN
F 1 "100k" H 7230 2595 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 7230 2550 50  0001 C CNN
F 3 "~" H 7300 2550 50  0001 C CNN
	1    7300 2550
	-1   0    0    1   
$EndComp
Wire Wire Line
	7500 3600 7500 4900
Wire Wire Line
	7700 4900 7700 3500
Wire Wire Line
	7900 4900 7900 3400
Wire Wire Line
	8100 4900 8100 3300
Wire Wire Line
	8200 4900 8200 3200
Wire Wire Line
	8000 3700 8500 3700
Connection ~ 8000 3700
Wire Wire Line
	8500 1900 8500 3700
Wire Wire Line
	4400 3100 7700 3100
Wire Wire Line
	7700 3100 7700 1900
Wire Wire Line
	7700 1900 7950 1900
Wire Wire Line
	8300 4900 8300 4450
Wire Wire Line
	4400 3600 7300 3600
Wire Wire Line
	4400 3500 6950 3500
Wire Wire Line
	4400 3400 6600 3400
Wire Wire Line
	4400 3300 6250 3300
Wire Wire Line
	4400 3200 5900 3200
Wire Wire Line
	7300 2250 7300 2400
Wire Wire Line
	4750 2250 5100 2250
Wire Wire Line
	7300 2700 7300 3600
Connection ~ 7300 3600
Wire Wire Line
	7300 3600 7500 3600
Wire Wire Line
	6950 2700 6950 3500
Connection ~ 6950 3500
Wire Wire Line
	6950 3500 7700 3500
Wire Wire Line
	6600 2700 6600 3400
Connection ~ 6600 3400
Wire Wire Line
	6600 3400 7900 3400
Wire Wire Line
	6250 2700 6250 3300
Connection ~ 6250 3300
Wire Wire Line
	6250 3300 8100 3300
Wire Wire Line
	5900 2700 5900 3200
Connection ~ 5900 3200
Wire Wire Line
	5900 3200 8200 3200
Wire Wire Line
	5900 2400 5900 2250
Connection ~ 5900 2250
Wire Wire Line
	5900 2250 6250 2250
Wire Wire Line
	6250 2400 6250 2250
Connection ~ 6250 2250
Wire Wire Line
	6250 2250 6600 2250
Wire Wire Line
	6600 2400 6600 2250
Connection ~ 6600 2250
Wire Wire Line
	6600 2250 6950 2250
Wire Wire Line
	6950 2400 6950 2250
Connection ~ 6950 2250
Wire Wire Line
	6950 2250 7300 2250
$Comp
L Device:R R3
U 1 1 60D83C70
P 5450 2550
F 0 "R3" H 5380 2504 50  0000 R CNN
F 1 "470" H 5380 2595 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 5380 2550 50  0001 C CNN
F 3 "~" H 5450 2550 50  0001 C CNN
	1    5450 2550
	-1   0    0    1   
$EndComp
$Comp
L Device:R R2
U 1 1 60D83CAC
P 5100 2550
F 0 "R2" H 5030 2504 50  0000 R CNN
F 1 "470" H 5030 2595 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 5030 2550 50  0001 C CNN
F 3 "~" H 5100 2550 50  0001 C CNN
	1    5100 2550
	-1   0    0    1   
$EndComp
Wire Wire Line
	5100 2250 5100 2400
Connection ~ 5100 2250
Wire Wire Line
	5100 2250 5450 2250
Wire Wire Line
	5450 2250 5450 2400
Connection ~ 5450 2250
Wire Wire Line
	5450 2250 5900 2250
Wire Wire Line
	5450 2700 5450 4100
Wire Wire Line
	5450 4100 7800 4100
Wire Wire Line
	7800 4100 7800 4900
Wire Wire Line
	5100 2700 5100 4300
Wire Wire Line
	5100 4300 7600 4300
Wire Wire Line
	7600 4300 7600 4900
Text Notes 8650 5050 0    50   ~ 0
Pins 9 and 10 are connected to the\ndrive mechanics motor that puts parts in the correct\nplaces for different drive functions:\ntape loading/ejecting, playing forward/reverse,\nfast forward/reverse.
Text Notes 5100 4600 0    50   ~ 0
These two lines provide power to a reflecting\nlight barrier at the reels to detect reel movement.\nThe phototransistor signal outputs are connected to J2.
$Comp
L power:+5V #PWR?
U 1 1 60D87109
P 4750 1300
F 0 "#PWR?" H 4750 1150 50  0001 C CNN
F 1 "+5V" H 4765 1473 50  0000 C CNN
F 2 "" H 4750 1300 50  0001 C CNN
F 3 "" H 4750 1300 50  0001 C CNN
	1    4750 1300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60D871B2
P 4750 4000
F 0 "#PWR?" H 4750 3750 50  0001 C CNN
F 1 "GND" H 4755 3827 50  0000 C CNN
F 2 "" H 4750 4000 50  0001 C CNN
F 3 "" H 4750 4000 50  0001 C CNN
	1    4750 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 4000 4750 3700
Connection ~ 4750 3700
Wire Wire Line
	4750 3700 8000 3700
Text Notes 4950 2150 0    50   ~ 0
470 Ohm resistors are used instead of 220 Ohm resistors,\nbecause the LEDs in the reflecting light barrier below the\nreels must not shine too bright. Otherwise the phototransistors\nwill just stop conducting.
Wire Wire Line
	4750 1300 4750 1650
Wire Wire Line
	7150 1650 4750 1650
Connection ~ 4750 1650
Wire Wire Line
	4750 1650 4750 2250
Wire Wire Line
	7450 1650 7700 1650
Wire Wire Line
	7700 1650 7700 1900
Connection ~ 7700 1900
Text Notes 7050 6800 0    50   ~ 0
This schematic describes a way to get all sensor outputs from the drive through one connector.\nContrary to the original PCB, the audio lines are not placed on the same PCB as the sensor outputs.
Text Notes 7000 6450 0    50   ~ 0
J1 is the short white bus cable on the bottom of the drive\nthat leads to a PCB inside the drive with reflecting light barriers\nand switches. Furthermore, the motor that controls the drive\nmechanics is connected to that bus cable, too.\n\nPinout:\n1 = Switch 1 (SW1), 2 = Light barrier 1 LED, 3 = Light barrier 1 Phototransistor\n4 = Light barrier 2 LED, 5 = Light barrier 2 Phototransistor,\n6 = GND\n7 = Switch 3 (SW3), 8 = Switch 2 (SW2)\n9 = Drive mechanics motor white cable\n10 = Drive mechanics motor black cable
$Comp
L Connector:Conn_01x02_Male J3
U 1 1 60D7B6CE
P 9950 4550
F 0 "J3" H 9923 4430 50  0000 R CNN
F 1 "Conn_01x02_Male" H 9923 4521 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Horizontal" H 9950 4550 50  0001 C CNN
F 3 "~" H 9950 4550 50  0001 C CNN
	1    9950 4550
	-1   0    0    1   
$EndComp
Wire Wire Line
	8400 4550 9750 4550
Wire Wire Line
	8400 4550 8400 4900
Wire Wire Line
	9750 4450 8300 4450
Text Notes 7800 1550 0    50   ~ 0
SW4 must be soldered to the backside of the PCB\nthat faces the drive in a certain position. It gets pressed\ndown when no cassette is in the drive. As soon as a cassette\nis pushed hard enough into the drive, it is released and stays\nreleased until the cassette is ejected.
$EndSCHEMATC
