# VW Gamma 3 sensor connector board

The schematic describes a circuit that passes the sensor outputs
from the drive onto a standard pin connector that can be connected
directly to a microcontroller that controls the drive motors.

## Drive information

The VW Gamma 3 car radio tape drive has several sensors in a PCB
inside the drive that give information about the drive's state.

Two reflecting light barriers at the reels allow the detection
of tape movement, end of tape and tape fault. The light barriers
work in "reverse", meaning that the phototransistors only conduct
when there is no light.

### Sensor PCB pinout

The sensor PCB is the board inside the drive that is connected
to the sensor connector board via a short white bus cable.

Pinout (as seen from the back of the drive, left to right):

- 1 = Switch 1 (SW1)
- 2 = Light barrier 1 LED
- 3 = Light barrier 1 Phototransistor
- 4 = Light barrier 2 LED
- 5 = Light barrier 2 Phototransistor
- 6 = GND
- 7 = Switch 3 (SW3)
- 8 = Switch 2 (SW2)
- 9 = Drive mechanics motor white cable
- 10 = Drive mechanics motor black cable

All switches and the phototransistores are connected to GND.

## Drive sensor functions

The switches are connected to VCC via 100 kOhm pull-up resistors.

The output frequency of the two light barriers can be as low as a
few hertz (takeup reel is loaded wit a lot of tape) and
65 hertz (fast forward).

### Switch 1 (SW1)

The function of this switch is currently unknown.
It changes state almost the same time as switch 2.

### Switch 2 (SW2)

Cassette-on-reel indicator: LOW level indicates the cassette is not
put onto the reels. HIGH level means the cassette has been put
onto the reels.

### Switch 3 (SW3)

Stateful drive state indicator. This switch has several functions and
its HIGH states must be counted to detect the current drive mechancs
state.

When spinning the drive mechanics motor in the direction to load a tape,
it gets to HIGH level several times after switch 1 and 2 have been switched
to HIGH levels and swich 3 itself switched from HIGH to LOW. After that,
each time switch 3 switching to HIGH level indicates a different mechanical
function that is available:

- 1st high level state: fast reverse (front reel spinning fast)
- 2nd high level state: fast forward (back reel spinning fast)
- 3rd high level state: playback forward (back reel as takeup reel)
- 4th high level state: playback reverse (forward reel as takeup reel, final state)

### Switch 4 (SW4)

Tape loaded indicator: When pressed (LOW level), no tape is loaded.
As soon as a tape is pushed hard enough into the drive, the switch
is released and its output will be pulled HIGH. It stays released
until the tape is ejected.

### Light barrier 1

Back reel motion detection.

__Note:__ The LED for this light barrier may need a greater resistor
than the LED for light barrier two. Otherwise the phototransistor
will not conduct at all. With only one working light barrier, end of
tape can be detected, but faults like a takeup reel failure or a
torn tape can only be detected for one of both playback directions.

### Light barrier 2

Front reel motion detection.
