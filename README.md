# open system compact cassette

The open system compact cassette (OSCC) project aims to improve compact cassette
playback by using modern microcontroller technology.

## Problems with compact cassette

### No speed adjustment, wow and flutter

The motor speed varies between compact cassette drives, leading to inaccurate
pitch during playback. Depending on the quality of the capstan flywheel and
the belt on common tape mechanisms, wow and flutter effects may be audible.

### Tape hiss

Tape hiss (high frequency noise) is audible, especially on cheap iron oxide
tapes. It is less audible on chrome dioxide and metal tapes and can be
suppressed even further using Dolby B, C or S noise reduction, but these
noise reduction systems aren't available on every player.

### Waste of bandwidth

Recordings on compact cassette are analog and must always be made at the
same speed. There is no official long play mode as with VHS that allows
putting the double amount of content onto a cassette. Speech doesn't need
the full bandwidth the standard playback speed provides. Other tape systems
such as microcassette used the same tape with lower speed to record speech
in a quality well enough for daily use.

## The proposed solutions for the compact cassette problems

The problems outlined above shall be solved during the development in the
open system compact cassette (OSCC) project.

### Automatic speed adjustment and speed control

Speed adjustment problems shall be solved using a low-amplitude 19 kHz tracking
signal that is recorded onto both linear tracks. A microcontroller can pick up
that signal, measure its frequency during playback and adjust the motor speed
so that that signal is played back at 19 kHz. The frequency of 19 kHz is chosen
for compatiblity with FM radio recordings where a 19 kHz pilot signal is used to
indicate FM stereo. Existing FM recordings on cassette where that 19 kHz signal
hasn't been filtered out could benefit from the automatic speed adjustment
functionality.

If the microcontroller can track changes in the frequency of the tracking signal
fast enough, the signal can also be used to reduce wow and flutter by quickly
adjusting the motor speed.

Using a rotary encoder (optical or mechanical) on the capstan and its flywheel,
the microcontroller can also measure the capstan speed and adjust the motor
speed quickly to let the capstan run at constant speed. This can also reduce
wow and flutter.

### Digital noise reduction

Tape hiss can be reduced by using digital noise reduction. During quiet segments
on the tape, the noise profile can be measured and noise filtering using that
profile can directly be applied. The same microcontroller that is responsible
for digital noise reduction could also be used to decode Dolby B, C or S
recordigs on tape drives that didn't have that functionality in its original
analog circuitry.


### Digital recordings for better bandwidth use

To use the storage space on a cassette more economically, digital recordings
shall be made possible using the Opus audio codec. Since only the bitrate
must be closely matched during playback, the tape speed may be lower than
standard speed during playback in cases, where low bitrates are used.
For speech recordings, the bitrate can be very low, meaning that possibly
hours of speech recordings may fit onto one side of the cassette.

Since digital audio can be buffered, speed variations as wow and flutter would
not have such a great impact on playback quality as with analog recordings.
If the data buffer is close to an underrun, the motor speed can be increased
to fill the buffer again. The opposite needs to be done if a buffer overflow
is about to happen.

With digital recodrings, a mode that goes into the direction of 8-track
cassettes could become possible with compact cassette. In that mode, each linear
track on each side would has its own digitally encoded stereo program, meaning
that in total, four stereo programs could be digitally recorded onto one compact
cassette.

## What this project aims to do

The goal of the open system compact cassette (OSCC) project is to provide
microcontroller program source code and circuit schematics for building an
open hardware and free libre open source software (FLOSS) based compact cassette
player or recorder.

The plan is to develop different modules for a cassette drive (motor control
unit, data encoding/decoding unit, amplifier unit etc.) that can be
combined as needed. Some simple cassette drives may not need a module to
control drive mechanics while others will definetly need such a module
to be able to load and unload a cassette.

This repository also contains some GNURadio experiments to test improvements to
analog compact cassette playback using software and to test storing digital data
on compact cassettes. The experiments are placed in the folder
"gnuradio-experiments".

## Repository structure

The unobvious folder names and their content:

- common: Files shared in several software or hardware modules.
- doc: Documentation
- drive-circuits: Schematics for circuits that are specific to certain
  cassette drives
- drive-unit: hard- and software for a cassette drive unit module
- motor-unit: hard- and software for a cassette drive motor unit module
- oscc-modem: A modulator/demodulator software for digital data on cassettes
- tools: Various tools related to cassettes and cassette drives

## Randomly answered questions (RAQ)

### Why build new hardware and software for compact cassettes?

1. To be able to replace parts from old cassette hardware when they cannot play
   cassettes (accurately) anymore.
2. Cassettes are having sort of a comeback (sales are going up in recent years)
   but good new hardware isn't easy to find.
3. Making compact cassettes and their drives useful again extends their product
   lifetime and by that can help the environment by reducing electronic
   and plastic waste.
