#!/bin/bash

# This is a very basic tracking signal encoder that outputs a sine wave
# on 19 kHz using the ffplay (from ffmpeg).
# A different sound card that the default card can be set using the
# environment variable AUDIODEV. For example:
#
#    AUDIODEV="default:CARD=USB" tracking-encoder-ffplay.sh
#
# If another sound system shall be used, it can be set
# using the SDL_AUDIODRIVER environment variable. For example:
#
#    SDL_AUDIODRIVER="othe-sound-system" tracking-encoder-ffplay.sh
#
# These both environment variables can be combined.

echo "Generating a tracking signal now. Press CRTL-C to stop this script."

ffplay -nodisp -loglevel fatal -f lavfi -vn -i "sine=frequency=19000" -volume 80
