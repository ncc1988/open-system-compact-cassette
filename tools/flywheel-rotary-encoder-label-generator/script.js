function generateSvg() {

    var form = document.forms['generate'];
    if (!form) {
        return;
    }

    var diameter = parseFloat(form.elements['diameter'].value);
    if ((diameter < 0.1) || (diameter > 99)) {
        return;
    }

    var parts = parseInt(form.elements['parts'].value);
    if ((parts < 1) || (parts > 1440)) {
        return;
    }

    var stroke_colour = form.elements['stroke_colour'].value;
    if (!stroke_colour) {
        return;
    }

    var inner_circle_diameter = parseFloat(form.elements['inner_circle_diameter'].value);
    if (inner_circle_diameter > diameter) {
        return;
    }

    var inner_circle_colour = form.elements['inner_circle_colour'].value;
    if (inner_circle_diameter && !inner_circle_colour) {
        return;
    }

    var quantity = parseInt(form.elements['quantity'].value);
    if (quantity < 1) {
        return;
    }

    var svg_frame = document.getElementById('svg_frame');
    if (!svg_frame) {
        return;
    }
    //Remove everything from inside the frame:
    while (svg_frame.firstChild) {
        svg_frame.removeChild(svg_frame.firstChild);
    }

    var svg_ns = 'http://www.w3.org/2000/svg';

    var svg_node = document.createElementNS(svg_ns, 'svg');

    svg_node.setAttribute('width', (diameter * 12) + 'mm');
    svg_node.setAttribute('height', (diameter * 12) + 'mm');

    var center = (diameter * 11) / 2;
    var radius = (diameter * 10) / 2;

    var group = document.createElementNS(svg_ns, 'g');
    group.setAttribute('fill', 'none');
    group.setAttribute('stroke', 'black');
    svg_node.appendChild(group);

    var part_angle = (360 / parts) * Math.PI / 180.0;
    var x = center + radius * Math.cos(part_angle);
    var y = center + radius * Math.sin(part_angle);

    for (var i = 1; i <= parts; i++) {
        var x2 = center + radius * Math.cos(part_angle * i);
        var y2 = center + radius * Math.sin(part_angle * i);
        var line = document.createElementNS(svg_ns, 'line');
        line.setAttribute('stroke', stroke_colour);
        line.setAttribute('x1', center + 'mm');
        line.setAttribute('y1', center + 'mm');
        line.setAttribute('x2', x2 + 'mm');
        line.setAttribute('y2', y2 + 'mm');
        group.appendChild(line);
    }

    //Add an inner circle, if it has a diameter > 0:
    if (inner_circle_diameter > 0) {
        var circle = document.createElementNS(svg_ns, 'circle');
        circle.setAttribute('cx', center + 'mm');
        circle.setAttribute('cy', center + 'mm');
        circle.setAttribute('r', (inner_circle_diameter * 5) + 'mm');
        circle.setAttribute('stroke', stroke_colour);
        circle.setAttribute('fill', inner_circle_colour);
        group.appendChild(circle);
    }

    if (quantity == 1) {
        //Add the generated SVG to the HTML document:
        svg_frame.appendChild(svg_node);
    } else {
        //Make clones from the generated SVG and attach them
        //to the HTML document:
        for (var i = 0; i < quantity; i++) {
            var clone = svg_node.cloneNode(true);
            svg_frame.appendChild(clone);
        }
    }
}
