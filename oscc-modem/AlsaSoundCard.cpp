/*
 * This file is part of the open-system-compact-cassette project.
 *
 * Copyright (C) 2020-2021 Moritz Strohm <ncc1988@posteo.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http s ://www.gnu.org/licenses/>.
 */


#include "AlsaSoundCard.h"


AlsaSoundCard::AlsaSoundCard(
    const std::string& sound_card_name,
    bool playback
    )
    :sound_card_name(sound_card_name),
     playback(playback)
{
}


void AlsaSoundCard::setChannels(uint16_t channels, bool force)
{
    if (!this->openCard()) {
        if (force) {
            throw OSCC::Exception(0, "error setting channels");
        } else {
            return;
        }
    }
    snd_pcm_hw_params_set_channels(
        this->sound_card,
        this->sound_card_parameters,
        channels
        );
}


void AlsaSoundCard::setSampleRate(uint32_t sample_rate, bool force)
{
    if (!this->openCard()) {
        if (force) {
            throw OSCC::Exception(0, "error setting sample rate");
        } else {
            return;
        }
    }

    int unit_direction = 0;
    unsigned int result_sample_rate = sample_rate;
    snd_pcm_hw_params_set_rate_near(
        this->sound_card,
        this->sound_card_parameters,
        &result_sample_rate,
        &unit_direction
        );
    if (force && (result_sample_rate != sample_rate)) {
        throw OSCC::Exception(
            0,
            fmt::format(
                "The sampling rate is {} instead of {}!",
                result_sample_rate,
                sample_rate
                )
            );
    }
}


bool AlsaSoundCard::openCard()
{
    if (this->sound_card == nullptr) {
        int result = snd_pcm_open(
            &(this->sound_card),
            this->sound_card_name.c_str(),
            SND_PCM_STREAM_PLAYBACK,
            0
            );
        if (result < 0) {
            return false;
        }
    }
    if (this->sound_card_parameters == nullptr) {
        snd_pcm_hw_params_alloca(&(this->sound_card_parameters));
        if (this->sound_card_parameters == nullptr) {
            return false;
        }

        snd_pcm_hw_params_set_access(
            this->sound_card,
            this->sound_card_parameters,
            SND_PCM_ACCESS_RW_INTERLEAVED
            );

        snd_pcm_hw_params_set_format(
            this->sound_card,
            this->sound_card_parameters,
            SND_PCM_FORMAT_S16_LE
            );
    }

    return true;
}
