/*
 * This file is part of the open-system-compact-cassette project.
 *
 * Copyright (C) 2020-2021 Moritz Strohm <ncc1988@posteo.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http s ://www.gnu.org/licenses/>.
 */


#ifndef OSCC__EXCEPTION
#define OSCC__EXCEPTION


#include <string>

#include <fmt/format.h>


namespace OSCC
{
    class Exception
    {
        protected:


        uint16_t error_code;


        std::string description;


        public:


        Exception(const uint16_t& error_code, const std::string& description)
            : error_code(error_code),
              description(description)
        {
            //Nothing else
        }


        std::string toString()
        {
            return fmt::format(
                "Error {}: {}",
                this->error_code,
                this->description
                );
        }


        uint16_t getErrorCode()
        {
            return this->error_code;
        }


        std::string getDescription()
        {
            return this->description;
        }
    };
}


#endif
