/*
 * This file is part of the open-system-compact-cassette project.
 *
 * Copyright (C) 2020-2021 Moritz Strohm <ncc1988@posteo.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http s ://www.gnu.org/licenses/>.
 */


#ifndef OSCC__MODEM_H
#define OSCC__MODEM_H


#include <cinttypes>
#include <complex>
#include <vector>


#include "liquid/liquid.h"


namespace OSCC
{
    class Modem
    {
        public:


        Modem(size_t packet_size = 4096);


        ~Modem();


        /**
         * Encodes binary data.
         */
        std::vector<std::complex<float>> encode(std::vector<uint8_t> data);


        /**
         * Decodes binary data.
         */
        std::vector<uint8_t> decode(std::vector<std::complex<float>> samples);


        protected:


        /**
         * The packetizer struct from liquidsdr.
         */
        //::packetizer packetizer;

        ::modem modem;


        //size_t encoded_data_max_size = 0;


        size_t packet_size = 0;


        /**
         * The current decoded bits. We cannot assume that the samples
         * we get during decoding are byte-aligned, so we have to store
         * all decoded bits in a temporary buffer until a full byte is read.
         */
        std::vector<uint8_t> decoded_bits = std::vector<uint8_t>(8);


        /**
         * decoded_bit_c is an index/counter for the current position in
         * the decoded_bits vector.
         */
        size_t decoded_bit_c = 0;
    };
}


#endif
