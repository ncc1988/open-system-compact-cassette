/*
 * This file is part of the open-system-compact-cassette project.
 *
 * Copyright (C) 2020-2021 Moritz Strohm <ncc1988@posteo.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http s ://www.gnu.org/licenses/>.
 */


#ifndef ALSASOUNDCARD_H
#define ALSASOUNDCARD_H


#include <cinttypes>
#include <cmath>
#include <iostream>
#include <memory>
#include <string>

#include <fmt/format.h>
#include <alsa/asoundlib.h>

#include "Exception.h"


class AlsaSoundCard
{
    public:


    AlsaSoundCard(const std::string& sound_card_name, bool playback = true);


    /**
     * @param bool force Whether to force the channels layout / amount (true)
     *    or not (false). The latter means that other amounts / layouts
     *    are acceptable.
     *
     * @throws std::exception If force is set to true and the
     *    requested channel layout / amount cannot be set, an exception is
     *    thrown.
     */
    void setChannels(uint16_t channels, bool force = true);


    /**
     * @param bool force Whether to force the samle rate (true)
     *    or not (false). The latter means that other sample rates
     *    are acceptable.
     *
     * @throws std::exception If force is set to true and the
     *    requested sample rate cannot be set, an exception is thrown.
     */
    void setSampleRate(uint32_t sample_rate, bool force = true);


    protected:


    /**
     * @returns True, if the sound card could be opened, false otherwise.
     */
    bool openCard();


    std::string sound_card_name;


    bool playback = true;


    snd_pcm_t* sound_card = nullptr;


    snd_pcm_hw_params_t* sound_card_parameters = nullptr;
};


#endif
