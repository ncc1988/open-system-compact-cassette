/*
 * This file is part of the open-system-compact-cassette project.
 *
 * Copyright (C) 2020-2021 Moritz Strohm <ncc1988@posteo.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http s ://www.gnu.org/licenses/>.
 */


#include <cmath>
#include <iostream>
#include <memory>
#include <string>
#include <vector>


#include <fmt/format.h>


#include "AlsaSoundCard.h"
#include "Exception.h"
#include "Modem.h"


void printHelp()
{
    //TODO
}

int main(int argc, char** argv)
{
    std::string sound_card_name = "default";

    if (argc > 1) {
        std::string arg1 = argv[1];
        if (arg1 == "--help") {
            printHelp();
            return EXIT_SUCCESS;
        } else {
            //Use the sound card name specified in arg1
            sound_card_name = arg1;
        }
    }

    std::string test_str = "Hello, tape world! Here is some digital data for you!";
    std::vector<uint8_t> test_data(test_str.begin(), test_str.end());

    auto modem = std::make_unique<OSCC::Modem>();


    auto result = modem->decode(
        modem->encode(
            test_data
            )
        );
    std::string result_str(result.begin(), result.end());

    std::cout << fmt::format("Test data:\t{0}\nResult:\t\t{1}", test_str, result_str) << std::endl;

    //Set up the access to the sound card and get its parameters.
    /*
    auto sound_card = std::make_unique<AlsaSoundCard>(sound_card_name);
    if (sound_card == nullptr) {
        std::cerr << "Error initialising sound card interface!" << std::endl;
        return EXIT_FAILURE;
    }
    try {
        sound_card->setChannels(1);
        sound_card->setSampleRate(48000, false);
    } catch (OSCC::Exception& e) {
        std::cerr << e.toString() << std::endl;
        return EXIT_FAILURE;
    }
    */
    return EXIT_SUCCESS;
}
