/*
 * This file is part of the open-system-compact-cassette project.
 *
 * Copyright (C) 2020-2021 Moritz Strohm <ncc1988@posteo.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http s ://www.gnu.org/licenses/>.
 */


#include "Modem.h"


using namespace OSCC;


Modem::Modem(size_t packet_size)
    : packet_size(packet_size)
{
    //this->packetizer = ::packetizer_create(packet_size, LIQUID_CRC_32, LIQUID_FEC_HAMMING74, LIQUID_FEC_NONE);
    this->modem = ::modem_create(LIQUID_MODEM_BPSK);
    //this->encoded_data_max_size = ::packetizer_get_enc_msg_len(this->packetizer);
}


Modem::~Modem()
{
    //::packetizer_destroy(this->packetizer);
    ::modem_destroy(this->modem);
}


std::vector<std::complex<float>> Modem::encode(std::vector<uint8_t> data)
{
    if (data.size() == 0) {
        return {};
    }
    std::vector<std::complex<float>> output;
    for (auto byte: data) {
        //Unpack the byte for BPSK (1 bit per symbol):
        std::vector<uint8_t> bits(8);
        bits[0] = byte & 0x01;
        bits[1] = byte & 0x02 >> 1;
        bits[2] = byte & 0x04 >> 2;
        bits[3] = byte & 0x08 >> 3;
        bits[4] = byte & 0x10 >> 4;
        bits[5] = byte & 0x20 >> 5;
        bits[6] = byte & 0x40 >> 6;
        bits[7] = byte & 0x80 >> 7;

        //Send the bits:
        for (auto bit: bits) {
            std::complex<float> sample;
            ::modem_modulate(this->modem, bit, &sample);
            output.push_back(sample);
        }
    }

    return output;
    //size_t output_size = ::packetizer_get_enc_msg_length(this->packetizer);
    //if (output_size <= 0) {
    //    return {};
    //}
    //output.resize(output_size);
    //::packetizer_encode(this->packetizer, data.data(), &output.front());
    //return output;
}


std::vector<uint8_t> Modem::decode(std::vector<std::complex<float>> samples)
{
    if (samples.size() == 0) {
        return {};
    }

    std::vector<uint8_t> output;

    for (auto sample: samples) {
        unsigned int bit = 0;

        ::modem_demodulate(this->modem, sample, &bit);
        this->decoded_bits[this->decoded_bit_c] = bit;
        if (this->decoded_bit_c == 7) {
            //We have one full byte.
            uint8_t byte = 0;
            for (uint8_t i = 0; i < 8; i++) {
                byte = byte | (this->decoded_bits[i] << i);
            }
            output.push_back(byte);
            this->decoded_bit_c = 0;
        } else {
            this->decoded_bit_c++;
        }
    }
    return output;
    /*
    TODO: rewrite for use with packetizer
    std::vector<uint8_t> output;
    size_t output_size = ::fec_get_dec_msg_length(LIQUID_FEC_HAMMING74, encoded_data.size());
    if (output_size <= 0) {
        return {};
    }
    output.resize(output_size);
    ::fec_decode(this->fec, encoded_data.size(), encoded_data.data(), output.data());

    return output;
    */
    return {};
}
