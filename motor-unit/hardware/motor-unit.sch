EESchema Schematic File Version 4
LIBS:motor-unit-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_Microchip_ATtiny:ATtiny45-20PU U1
U 1 1 5DD84877
P 4150 3050
F 0 "U1" H 3620 3096 50  0000 R CNN
F 1 "ATtiny45-20PU" H 3620 3005 50  0000 R CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 4150 3050 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/atmel-2586-avr-8-bit-microcontroller-attiny25-attiny45-attiny85_datasheet.pdf" H 4150 3050 50  0001 C CNN
	1    4150 3050
	1    0    0    -1  
$EndComp
Text Label 5350 2450 0    50   ~ 0
lower_speed_signal
Text Label 6350 2450 0    50   ~ 0
higher_speed_signal
Text Label 6400 2850 0    50   ~ 0
motor_pwm_signal
$Comp
L Regulator_Linear:L7805 U2
U 1 1 60A2D51C
P 2200 1900
F 0 "U2" H 2200 2142 50  0000 C CNN
F 1 "TSR1-2450" H 2200 2051 50  0000 C CNN
F 2 "" H 2225 1750 50  0001 L CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/41/4f/b3/b0/12/d4/47/88/CD00000444.pdf/files/CD00000444.pdf/jcr:content/translations/en.CD00000444.pdf" H 2200 1850 50  0001 C CNN
	1    2200 1900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60A2D76B
P 1550 3900
F 0 "#PWR?" H 1550 3650 50  0001 C CNN
F 1 "GND" H 1555 3727 50  0000 C CNN
F 2 "" H 1550 3900 50  0001 C CNN
F 3 "" H 1550 3900 50  0001 C CNN
	1    1550 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 3650 5350 3650
Wire Wire Line
	8300 3650 8300 3050
Connection ~ 4150 3650
$Comp
L Motor:Motor_DC M1
U 1 1 60A2E59A
P 8300 1850
F 0 "M1" H 8458 1846 50  0000 L CNN
F 1 "Motor_DC" H 8458 1755 50  0000 L CNN
F 2 "" H 8300 1760 50  0001 C CNN
F 3 "~" H 8300 1760 50  0001 C CNN
	1    8300 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	8300 2650 8300 2150
Wire Wire Line
	8300 1250 8300 1650
Connection ~ 1550 1250
Wire Bus Line
	1550 1250 1600 1250
$Comp
L Device:CP C1
U 1 1 60A306E5
P 1550 2500
F 0 "C1" H 1668 2546 50  0000 L CNN
F 1 "470μF" H 1668 2455 50  0000 L CNN
F 2 "" H 1588 2350 50  0001 C CNN
F 3 "~" H 1550 2500 50  0001 C CNN
	1    1550 2500
	1    0    0    -1  
$EndComp
Connection ~ 2200 3650
Wire Wire Line
	2200 3650 1550 3650
Wire Wire Line
	2200 3650 2700 3650
$Comp
L Device:CP C2
U 1 1 60A317A3
P 2700 2650
F 0 "C2" H 2818 2696 50  0000 L CNN
F 1 "10μF" H 2818 2605 50  0000 L CNN
F 2 "" H 2738 2500 50  0001 C CNN
F 3 "~" H 2700 2650 50  0001 C CNN
	1    2700 2650
	1    0    0    -1  
$EndComp
Connection ~ 2700 3650
Wire Wire Line
	2700 3650 4150 3650
Wire Wire Line
	2200 2200 2200 3650
Wire Wire Line
	1550 1250 1550 1900
Wire Wire Line
	1550 1900 1900 1900
Connection ~ 1550 1900
Wire Wire Line
	1550 2650 1550 3650
Wire Wire Line
	1550 1900 1550 2350
Wire Wire Line
	2500 1900 2700 1900
Wire Wire Line
	4150 1900 4150 2450
Wire Wire Line
	2700 1900 2700 2500
Connection ~ 2700 1900
Wire Wire Line
	2700 1900 4150 1900
Wire Wire Line
	2700 2800 2700 3650
$Comp
L Switch:SW_Push SW2
U 1 1 60A35439
P 6350 2100
F 0 "SW2" V 6304 2248 50  0000 L CNN
F 1 "SW_FASTER" V 6395 2248 50  0000 L CNN
F 2 "" H 6350 2300 50  0001 C CNN
F 3 "" H 6350 2300 50  0001 C CNN
	1    6350 2100
	0    1    1    0   
$EndComp
$Comp
L Switch:SW_Push SW1
U 1 1 60A35753
P 5350 2100
F 0 "SW1" V 5304 2248 50  0000 L CNN
F 1 "SW_SLOWER" V 5395 2248 50  0000 L CNN
F 2 "" H 5350 2300 50  0001 C CNN
F 3 "" H 5350 2300 50  0001 C CNN
	1    5350 2100
	0    1    1    0   
$EndComp
Wire Wire Line
	5350 1900 5000 1900
Connection ~ 4150 1900
Wire Wire Line
	4750 3050 5350 3050
Wire Wire Line
	5350 3050 5350 2300
Wire Wire Line
	6350 3150 6350 2300
$Comp
L Device:R R1
U 1 1 60A36362
P 5350 3400
F 0 "R1" H 5200 3450 50  0000 L CNN
F 1 "33k" H 5150 3350 50  0000 L CNN
F 2 "" V 5280 3400 50  0001 C CNN
F 3 "~" H 5350 3400 50  0001 C CNN
	1    5350 3400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 60A36684
P 6350 3400
F 0 "R2" H 6420 3446 50  0000 L CNN
F 1 "33k" H 6420 3355 50  0000 L CNN
F 2 "" V 6280 3400 50  0001 C CNN
F 3 "~" H 6350 3400 50  0001 C CNN
	1    6350 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 3550 5350 3650
Connection ~ 5350 3650
Wire Wire Line
	5350 3250 5350 3050
Connection ~ 5350 3050
Wire Wire Line
	5350 3650 5600 3650
Wire Wire Line
	6350 3150 6350 3250
Connection ~ 6350 3150
Wire Wire Line
	6350 3550 6350 3650
Connection ~ 6350 3650
Wire Wire Line
	6350 3650 8300 3650
Wire Wire Line
	5350 1900 6350 1900
Connection ~ 5350 1900
Text Notes 5000 1800 0    50   ~ 0
Work in progress: The switches shall be replaced\nonce the PWM motor control is working.
Text Notes 6650 3000 0    50   ~ 0
PWM frequency: 126.5 kHz
Wire Wire Line
	1550 3650 1550 3900
Connection ~ 1550 3650
Wire Wire Line
	1550 1250 8300 1250
Text Label 5500 2950 0    50   ~ 0
!motor_enable_signal
$Comp
L Device:R R?
U 1 1 60AAF22E
P 5000 2450
F 0 "R?" H 5070 2496 50  0000 L CNN
F 1 "220k" H 5070 2405 50  0000 L CNN
F 2 "" V 4930 2450 50  0001 C CNN
F 3 "~" H 5000 2450 50  0001 C CNN
	1    5000 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 2950 5000 2950
$Comp
L Connector:Conn_01x02_Male J1
U 1 1 60AB04FC
P 5600 4200
F 0 "J1" V 5800 4300 50  0000 L CNN
F 1 "Conn_01x02_Male" V 5700 4300 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 5600 4200 50  0001 C CNN
F 3 "~" H 5600 4200 50  0001 C CNN
	1    5600 4200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5600 4000 5600 3650
Connection ~ 5600 3650
Wire Wire Line
	4750 3150 6350 3150
Wire Wire Line
	5700 4000 5700 2950
Text Notes 5800 4050 0    50   ~ 0
If the cassette drive has a switch to indicate a loaded cassette,\nthat switch can be connected to this connector to tell the\nmicrocontroller that the motor can be started.
Wire Wire Line
	5600 3650 6350 3650
Wire Wire Line
	5000 2600 5000 2950
Connection ~ 5000 2950
Wire Wire Line
	5000 2950 5700 2950
Wire Wire Line
	5000 2300 5000 1900
Connection ~ 5000 1900
Wire Wire Line
	5000 1900 4150 1900
$Comp
L power:VCC #PWR?
U 1 1 60ABE439
P 1550 1050
F 0 "#PWR?" H 1550 900 50  0001 C CNN
F 1 "VCC" H 1567 1223 50  0000 C CNN
F 2 "" H 1550 1050 50  0001 C CNN
F 3 "" H 1550 1050 50  0001 C CNN
	1    1550 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 1050 1550 1250
Text Notes 1750 1100 0    50   ~ 0
VCC must be at least 6.5 Volts. If the motor has no trimmer\nfor adjusting the speed, VCC should be high enough so that\nthe motor reaches standard tape speed at about\n90% PWM duty cycle.
Wire Wire Line
	4750 2850 8000 2850
$Comp
L Device:Q_NMOS_GDS Q1
U 1 1 60ABF6CF
P 8200 2850
F 0 "Q1" H 8405 2896 50  0000 L CNN
F 1 "SMP50N06" H 8405 2805 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 8400 2950 50  0001 C CNN
F 3 "~" H 8200 2850 50  0001 C CNN
	1    8200 2850
	1    0    0    -1  
$EndComp
$EndSCHEMATC
