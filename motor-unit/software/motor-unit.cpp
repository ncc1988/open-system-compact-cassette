/*
 * open-system-compact-cassette motor control
 *
 * Copyright (C) 2019-2021 Moritz Strohm
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http s ://www.gnu.org/licenses/>.
 */


#define F_CPU 1000000L


#include <avr/eeprom.h>
#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>


/*
 * This program is designed for AVR microcontrollers and the ATtiny45
 * in particular.
 */


// Globals


constexpr bool DEBUG_MODE = true;


/**
 * This global variable holds the current motor speed (PWM duty cycle).
 * For safety reasons, it is initialised with zero, meaning no motion.
 */
volatile uint8_t current_motor_speed = 0;


/**
 * The amount of time for a motor "noise" signal period.
 */
volatile uint16_t current_motor_period = 0;


/**
 * The amount of time for the tracking signal period.
 */
volatile uint16_t current_tracking_period = 0;


/**
 * The frequency of the tracking signal.
 * By default, this is 19000Hz.
 */
constexpr uint16_t TRACKING_SIGNAL_FREQUENCY = 19000;


/**
 * The period of the tracking signal.
 */
constexpr uint16_t TRACKING_SIGNAL_PERIOD = 1/TRACKING_SIGNAL_FREQUENCY;


// Motor speed control functions:


/**
 * Starts a new motor speed measurement cycle.
 */
void startMotorSpeedMeasurement()
{
    
}


/**
 * This function checks the motor speed by checking the time
 * from the last motor "noise" signal period to the current one.
 */
void checkMotorSpeed()
{
    
}


/**
 * Sets the PWM duty cycle for the motor output
 * and by that setting the motor speed.
 */
void setMotorSpeed(uint8_t power)
{
    current_motor_speed = power;
    OCR1A = current_motor_speed;
}


//Tracking functions:


/**
 * Starts a new tracking signal measurement cycle.
 *
 * @see checkCalibrationSignal
 */
void startTrackingSignalMeasurement()
{
    
}


/**
 * Checks the frequency of the tracking signal and adjusts
 * the default motor speed (PWM duty cycle).
 *
 * The tracking signal must be a continues signal from the tape around
 * 19 kHz to be recognised.
 */
void checkTrackingSignal()
{
    
}


// Settings handling functions:


/**
 * Load the motor speed from EEPROM.
 */
void loadMotorSpeed()
{
    //Load two bytes from the EEPROM.
    //The first byte contains a "magic" value
    //(0xCC for Compact Cassette)
    //while the second contains the speed.
    uint16_t speed_data = eeprom_read_word(reinterpret_cast<uint16_t*>(0));
    uint8_t magic = speed_data >> 8;
    if (magic == 0xCC) {
        //A valid speed value is present.
        current_motor_speed = speed_data & 0x00FF;
    } else {
        //Probably uninitialised memory.
        //In this case we start with a speed value of 32.
        //This value is not saved as it is probaly inaccurate
        //and needs adjusting it to a good value before saving it.
        current_motor_speed = 32;
    }
}


/**
 * Save the motor speed to EEPROM.
 */
void saveMotorSpeed()
{
    uint8_t magic = eeprom_read_byte(reinterpret_cast<uint8_t*>(0));
    if (magic != 0xCC) {
        //Uninitialised memory. We must first write the magic value.
        eeprom_write_byte(reinterpret_cast<uint8_t*>(0), 0xCC);
    }
    //Now we can write the motor speed:
    eeprom_write_byte(reinterpret_cast<uint8_t*>(1), current_motor_speed);
}


// Motor control functions:


/**
 * Sets up the timer for PWM and starts outputting a PWM
 * with the duty cycle specified in current_motor_speed.
 */
void startMotor()
{
    //Set the PWM frequency to 126.5 kHz using the PLL as clock source
    //for timer 1:
    TCCR1 = (1 << CTC1) | (1 << PWM1A) | (1 << COM1A1) | (0 << COM1A0) | (0 << CS13) | (0 << CS12) | (1 << CS11) | (0 << CS10);
    PLLCSR = (0 << LSM) | (1 << PCKE) | (1 << PLLE) | (1 << PLOCK);

    //We need as much PWM steps as possible, so OCR1C is set to 255:
    OCR1C = 0xFF;

    //Disable all timer interrupts:
    TIMSK = 0x00;

    //Set the initial motor speed:
    OCR1A = current_motor_speed;
}


/**
 * Sets the PWM duty cycle to current_motor_speed.
 */
void applyMotorSpeed()
{
    OCR1A = current_motor_speed;
}


// Setup functions:


/**
 * Setup interrupts for checking functions.
 */
void setupInterrupts(bool with_calibration = false)
{
    //
}


// Main function:


int main(void)
{
    DDRB |= (1 << PB1); //PB1 is our PWM output for the motor.
    cli();
    loadMotorSpeed();
    setupInterrupts();
    sei();
    startMotor();

    while (true) {
        //Check the manual speed control buttons:
        bool increase_button_pressed = PINB & (1 << PB3);
        bool decrease_button_pressed = PINB & (1 << PB4);

        if (increase_button_pressed && decrease_button_pressed) {
            //Start tracking
            startTrackingSignalMeasurement();
        } else if (increase_button_pressed) {
            if (current_motor_speed < 0xFF) {
                current_motor_speed++;
                applyMotorSpeed();
            }
        } else if (decrease_button_pressed) {
            if (current_motor_speed > 0x00) {
                current_motor_speed--;
                applyMotorSpeed();
            }
        }
        _delay_ms(50);
    }
}
