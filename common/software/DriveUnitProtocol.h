/*
 * This file is part of open-system-compact-cassette
 *
 * Copyright (C) 2019-2020 Moritz Strohm
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http s ://www.gnu.org/licenses/>.
 */


#ifndef DRIVEPROTOCOL_H
#define DRIVEPROTOCOL_H


#include <cinttypes>


namespace OSCC
{
    enum DriveState
    {
        /**
         * The drive is in idle state and ready to load a tape.
         */
        READY = 0,

        /**
         * A cassette is being loaded into the drive.
         */
        LOAD = 1,

        /**
         * A cassette has been loaded into the drive.
         */
        LOADED = 2,

        /**
         * A cassette is being ejected from the drive.
         * After the cassette is ejected, the drive
         * should return to the READY state.
         */
        EJECT = 3,

        /**
         * The drive is in play/recording mode.
         */
        PLAY = 10,

        /**
         * The drive is in pause mode.
         */
        PAUSE = 11,

        /**
         * The drive is playing in reverse mode.
         */
        PLAYREVERSE = 12,

        /**
         * The drive is in fast forwarding mode.
         */
        FFORWARD = 13,

        /**
         * The drive is in fast reverse mode.
         */
        FREVERSE = 14,

        /**
         * The drive detected an error.
         */
        ERROR = 100,

        /**
         * The drive detected a fatal error and halted.
         */
        ERRORHALT = 101
    };


    enum ErrorCode
    {
        /**
         * No error occurred.
         */
        NO_ERROR = 0,

        /**
         * Capstan failure (no tape movement detected).
         */
        CAPSTAN_FAILURE = 1,

        /**
         * The takeup reel is blocked (not moving).
         */
        TAKEUP_REEL_BLOCKED = 2,

        /**
         * The tape is stuck (cannot be moved).
         */
        TAPE_STUCK = 3
    };


    /**
     * The message types to be exchanged between the
     * control unit and the drive unit.
     */
    enum DriveMessageTypes
    {
        /**
         * State information sent by the drive unit.
         */
        STATE_INFO = 0,

        /**
         * The control unit issues an command to the drive unit.
         */
        SET_MODE = 1,

        /**
         * The control unit requests the state of the drive unit.
         */
        GET_STATE = 2
    };


    /**
     * The DriveUnitProtocol class defines the communication protocol
     * between the drive unit and the control unit.
     */
    class DriveUnitProtocol
    {
        public:

        /**
         * The checksum for the following five bytes.
         */
        uint8_t checksum;

        /**
         * The message type. Depending on the message type,
         * the amount of bytes that follow this byte is varying
         * and can be up to four bytes.
         * @see enum DriveMessageTypes
         */
        uint8_t message_type;

        /**
         * The message data.
         */
        uint32_t message_data;
    };
}


#endif
